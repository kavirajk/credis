#ifndef MACRO_H
#define MACRO_H

#define REPLY_STRING 0
#define REPLY_ERROR  1
#define REPLY_INT    2
#define REPLY_BULK   3
#define REPLY_ARRAY  4

#endif
