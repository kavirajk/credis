#include "parser.h"
#include "macro.h"
#include <sstream>
#include <cassert>

using namespace std;

void parser::serialize(string& data) {
  size_t ar_len=0,ob_len=0;

  for(int i=0;i<data.size();++i) {
    if(data[i]==' ')
      ar_len++;
  }
  ar_len++; // no of words is more than no of white space by 1
  
  string serialized="";
  serialized+='*';
  serialized+=to_string(ar_len);
  add_CRLF(serialized);

  stringstream ss(data);
  string word;
  while(ss>>word) {
    serialized+='$';
    serialized+=to_string(word.size());
    add_CRLF(serialized);
    serialized+=word;
    add_CRLF(serialized);
  }
  data=serialized;
}

void parser::add_CRLF(string& s) {
  s+='\r';
  s+='\n';
}

void parser::dserialize(string& s,redis_reply_obj& obj) {
  memset((void*)&obj,0,sizeof(obj));
  int i=0;
  switch(s[0]) {
  case '+':
    obj.type=REPLY_STRING;
    handle_string(s,obj);
    break;
  case '-':
    obj.type=REPLY_ERROR;
    handle_string(s,obj);
    break;
  case ':':
    obj.type=REPLY_INT;
    handle_string(s,obj);
    break;
  case '$':
    obj.type=REPLY_BULK;
    handle_string(s,obj);
    break;
  case '*':
    obj.type=REPLY_ARRAY;
    handle_array(s,i,obj);
    break;
  default:
    assert(NULL);
  }
}

string parser::handle_any_string(const string& s) {
  int i=1;
  if(s[0]=='$') {
    while(s[i]!='\r')
      ++i;
    ++i;
  }
  string str="";
  while(s[i]!='\r') {
    str+=s[i++];
  }
  str+='\0';
  return str;
}

void parser::handle_string(string& s,redis_reply_obj& robj) {
  robj.str=handle_any_string(s);
}

void parser::handle_array(string& s,int& idx,redis_reply_obj& robj) {
  int c=0;
  idx+=1; // skip '*' char

  while(s[idx]!='\r') {
    c=c*10+(s[idx++]-'0');
  }

  idx+=2; // skip \r\n

  for(int i=0;i<c;++i) {
    if(s[idx]=='*') {
      handle_array(s,idx,robj);
    } else {
      ++idx; // skip delimeter
      string res="";
      while(s[idx]!='\r') {
	res+=s[idx++];
      }
      robj.array.push_back(res);
      idx+=2; //skip \r\n
    }
  }
}
