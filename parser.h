#ifndef PARSER_H
#define PARSER_H

#include <iostream>
#include <vector>
#include <cstring>

using namespace std;

struct redis_reply_obj {
  int type;
  string str;
  int nval;
  vector<string> array;
};

class parser {
 public:
  void serialize(string& s);
  void dserialize(string& s,redis_reply_obj& robj);
 private:
  void add_CRLF(string& s);
  void handle_string(string& s, redis_reply_obj& robj);
  string handle_any_string(const string& s);
  void handle_array(string& s,int& i,redis_reply_obj& robj);
};

#endif
