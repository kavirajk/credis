#ifndef CREDIS_H
#define CREDIS_H

#include "rerror.h"
#include "parser.h"


using namespace std;

class credis {

 public:
  credis(string host,int port);
  int set(string key,string value);
  string get(string key);
  ~credis();

 private:
  int sd,port;
  parser pr;
  string host;
  int create_redis_sock();
  int bind_redis_sock();
  int connect_redis_sock();
  int redis_send(string data);
  string redis_receive();
};

#endif
