#include "credis.h"
#include "rerror.h"
#include "macro.h"
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <cstring> //memset and memcpy
#include <netdb.h>

using namespace std;

credis::credis(string host,int port):
  host(host),
  port(port),pr() {
  create_redis_sock();
  bind_redis_sock();
  connect_redis_sock();
}

int credis::create_redis_sock() {
  sd=socket(AF_INET,SOCK_STREAM,0);
  if(sd<0)
    return REDIS_ERR;
  return REDIS_OK;
}

int credis::bind_redis_sock() {
  struct sockaddr_in client;
  memset((void*)&client,0,sizeof(client));
  
  /*getting ip from hostname
  struct hostent *hp;
  bzero(hp,sizeof(hp));
  hp=gethostbyname(host);
  if(!hp) {
    fprintf(stderr,"unable to resolve hostname %s\n",host);
    return REDIS_ERR;
  }

  
  memcpy(&client.sin_addr,hp->h_addr_list[0],hp->h_length);*/
  client.sin_family=AF_INET;
  client.sin_port=htons(0);
  client.sin_addr.s_addr=htonl(INADDR_ANY);
  
  if(bind(sd,(struct sockaddr*)&client,sizeof(client))<0) {
    fprintf(stderr,"unable to bind to socket");
    return REDIS_ERR;
  }
  
  return REDIS_OK;
}

int credis::connect_redis_sock() {
  struct sockaddr_in server;
  memset((void*)&server,0,sizeof(server));

  struct hostent *hp;
  //  bzero(hp,sizeof(hp));

  hp=gethostbyname(host.c_str());
  if(!hp) {
    fprintf(stderr,"HOST_ERROR: unable to resolve hostname %s\n",host.c_str());
    return REDIS_ERR;
  }

  memcpy(&server.sin_addr,hp->h_addr_list[0],hp->h_length);
  server.sin_family=AF_INET;
  server.sin_port=htons(port);

  if(connect(sd,(struct sockaddr*)&server,sizeof(server))<0) {
    fprintf(stderr,"CONNECT_ERROR: unable to connect to hostname %s over the port %d\n",host.c_str(),port);
    return REDIS_ERR;
  }

  return REDIS_OK;
}

int credis::redis_send(string data) {
  pr.serialize(data);
  if(send(sd,data.c_str(),data.size(),0)<0) {
    fprintf(stderr,"SEND_ERROR: unable to send data to hostname %s\n",host.c_str());
    return REDIS_ERR;
  }
  return REDIS_OK;
}

int credis::set(string key,string value) {
  string data="set";
  data+=" "+key+" "+value;
  if(redis_send(data)!=REDIS_OK) {
    return REDIS_ERR;
  }
  string rdata=redis_receive();
  redis_reply_obj ro;
  pr.dserialize(rdata,ro);
}

string credis::get(string key) {
  string data="get";
  data+=" " + key;
  if(redis_send(data)!=REDIS_OK) {
    return "";
  }
  string rdata=redis_receive();
  redis_reply_obj ro;
  pr.dserialize(rdata,ro);
  return ro.str;
}

string credis::redis_receive() {
  string rcv="";
  int nbytes;
  char buffer[1024*16];
  while((nbytes=recv(sd,buffer,sizeof(buffer),0))) {
    rcv+=string(buffer);
    //    cout<<"nbytes: "<<nbytes<<endl;
    if(nbytes<1024*16)
      break;
  }
  return rcv;
}

credis::~credis() {
  shutdown(sd,SHUT_RDWR);
}
