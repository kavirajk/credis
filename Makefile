CC = g++
CFLAGS = -std=c++11

all: test_credis.out

test_credis.out: test_credis.o parser.o credis.o
	$(CC) test_credis.o parser.o credis.o $(CFLAGS) -o test_credis.out

test_credis.o: test_credis.cc
	$(CC) -c test_credis.cc $(CFLAGS)

parser.o: parser.cc
	$(CC) -c parser.cc $(CFLAGS)

credis.o: credis.cc
	$(CC) -c credis.cc $(CFLAGS)

clean:
	rm -rf *.out *.o
