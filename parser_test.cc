#include "parser.h"

using namespace std;

int main(int argc,char** argv) {
  string s="";
  for(int i=1;i<argc;i++) {
    s+=string(argv[i]);
    if(i!=argc-1)
      s+=" ";
  }
  parser p;
  cout<<"sent: "<<s<<endl;
  p.serialize(s);
  cout<<"received: "<<s<<endl;
  return 0;
}
